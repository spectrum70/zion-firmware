/*
 * minilibc
 *
 * (C) 2017 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of minilibc firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

long __sysc_get_heap_start(void)
{
	register int rval __asm__("r0");

	asm volatile("swi 0" : "=r"(rval) :);

	return rval;
}

int __sysc_get_heap_size(void)
{
	register int rval __asm__("r0");

	asm volatile("swi 1" : "=r"(rval) :);

	return rval;
}
