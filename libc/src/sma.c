/*
 * minilibc
 *
 * (C) 2017 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of minilibc firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <sma.h>
#include <string.h>
#include <syscalls.h>

/* Avoid multiple syscalls */
static long heap_start;
static int heap_size;

/*
 * Implementing a very simple memory allocator (smalloc)
 *
 * Defining a minimal 16bits block header.
 *
 * - max block size: 2^14 : (16KB - 1) max block
 * - 4 flags (2 bits)
 *
 * Idea is that once reused a free slot, another mark is created just after
 * defining a minor size free block.
 */

enum {
	MBLOCK_FREE,
	MBLOCK_USED,
};

struct mblock {
	uint16_t flags:2;
	uint16_t size:14;
} __attribute__((packed));

static void sma_block_setup(struct mblock *mb, uint16_t size, char flags)
{
	mb->size = size;
	mb->flags = flags;
}

char * sma_get_free_slot(uint16_t size)
{
	char *p = (char *)heap_start;
	char *end = p + heap_size;

	if (size > heap_size)
		return 0;

	for (;;) {
		/* Check for a block */
		if (!*p) {
			/* Free heap area */
			if ((p + size + sizeof(struct mblock)) < end) {
				sma_block_setup((struct mblock *)p,
						size, MBLOCK_USED);
				break;
			}
			/* Space finished */
			return 0;
		} else {
			struct mblock *mb = (struct mblock *)p;

			if (mb->flags == MBLOCK_FREE) {
				if (mb->size <= size) {
					size_t rest = mb->size - size;

					if (rest > sizeof(struct mblock)) {
						char *next = p +
							sizeof(struct mblock) +
							mb->size;

						mb = (struct mblock *)next;
						sma_block_setup(mb, rest -
							sizeof(struct mblock),
							MBLOCK_FREE);
					}
					break;
				}
			}
			/* Block was busy or too small */
			p += (mb->size + sizeof(struct mblock));
			/* Space full ? */
			if (p > (end - sizeof(struct mblock)))
				return 0;
		}
	}

	return (p + sizeof(struct mblock));
}

void sma_free(char *p)
{
	if (!p)
		return;

	p -= sizeof(struct mblock);

	if (*p) {
		struct mblock *mb = (struct mblock *)p;

		/* cleanup flags, means free */
		mb->flags = MBLOCK_FREE;
	}
}

void sma_init(void)
{
	if (!heap_start) {
		/* First init */
		heap_start = __sysc_get_heap_start();
		heap_size = __sysc_get_heap_size();
		memset((char*)heap_start, 0, heap_size);
	}
}