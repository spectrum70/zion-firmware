/*
 * minilibc
 *
 * (C) 2017 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of minilibc firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdint.h>
#include <sma.h>

int abs(int n)
{
	const int ret[2] = { n, -n };

	return ret[n < 0];
}

void *malloc(size_t size)
{
	static char allocator_init = 0;

	if (!allocator_init) {
		allocator_init++;
		sma_init();
	}

	return sma_get_free_slot(size);
}

void free(void *ptr)
{
	sma_free(ptr);
}
