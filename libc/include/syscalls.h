#ifndef __libc_syscalls_h
#define __libc_syscalls_h

/* libc bindings to the OS */

int __sysc_get_heap_size(void);
long __sysc_get_heap_start(void);

#endif /* __libc_syscalls_h */