#ifndef __stdlib_h
#define __stdlib_h

int abs(int j);
void *malloc(size_t size);
void free(void *ptr);

#endif /* __stdlib_h */
