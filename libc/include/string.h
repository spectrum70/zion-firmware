#ifndef __string_H
#define __string_H

#include <stdint.h>

void *memset(void *ptr, int value, size_t num);
void *memcpy(void *dest, const void *src, size_t num);
size_t strlen(const char *s);
char *strcpy(char *dest, const char *src);
char *strncpy(char *dest, const char *src, size_t n);

#endif /* __string_H */
