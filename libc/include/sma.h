#ifndef __sma_H
#define __sma_H

#include <stdint.h>

char * sma_get_free_slot(uint16_t size);
void sma_free(char *p);
void sma_init(void);

#endif /* __sma_H */
