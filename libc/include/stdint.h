#ifndef __libc_stdint_h
#define __libc_stdint_h

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned long uint32_t;
typedef unsigned long long uint64_t;

typedef unsigned long size_t;

#endif /* __libc_stdint_h */