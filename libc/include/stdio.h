#ifndef __stdio_h
#define __stdio_h

#ifdef __GNUC__
typedef __builtin_va_list va_list;
#endif

int printf(char* s, ...);

typedef void (*std_output)(char *s);

void set_std_output(std_output func);


#endif /* __stdio_h */
