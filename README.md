# Zion Firmware - a compact FOSS cortex-m4 firmware framework

SPDX-License-Identifier: GPL-2.0+


(C) Copyright 2020
Angelo Dureghello <angelo.dureghello AT timesys.com>.

## Introduction

This projects is a full (actually stm32) cortex-m4 development framework,
written totally from scratch, from start.s and linker scripts, without reusing
any ST library part.

Main purpose was to enjoy, to have a final binary as small as possible, and
from this target, all makefiles are strongly tuned for this. 

The framework is composed by:

- a small libc (libc folder),
- a system and drivers library, startup files included (lib),
- a set of demo bare-metal firmware projects, as those in thermo or test,
  linking against lib and libc.


## Build all the libraries

From root directory, export the cortex-m4 toolchian path:

`
export CROSS_COMPILE=/opt/toolchains/arm/gcc-arm-8.3-2019.03-....
make clean
make
`

This will build all the needed libraries.

## Build specific firmware

`
cd firmware/carlights
make clean
make
`

## Program the cortex-m4 internal flash

Programming is now supported, using st-link v2 right now, and openocd. 
Plase check am existing firmware makefile, and use proper linker script.

To program, use:

`make burn`

or run

`./openocd.sh`

from the firmware directory.

## Reference board

This firmware has been tested in the "token" style reference board, please
see refboard directory for Kicad schematics.


Thanks for using (or considering using) it.
Please provide any feedback to angelo70 AT gmail.com
