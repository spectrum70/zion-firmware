# SPDX-License-Identifier: GPL-2.0

VERSION = 1
NAME = Angelo Dureghello

# General setup, recursive build
export MCU_MODEL=STM32F303K8
export MCU_OPTS=-mfpu=fpv4-sp-d16 -mfloat-abi=hard

export MCU=cortex-m4
export CC=$(CROSS_COMPILE)gcc
export AR=$(CROSS_COMPILE)ar

export ASFLAGS=-mcpu=$(MCU) -mthumb -Wall -Os -D__ASSEMBLY__
export CFLAGS=-mcpu=$(MCU) \
	-static \
	-ffreestanding \
	-nostartfiles \
	-nostdlib \
	-fdata-sections \
	-ffunction-sections \
	-Wall -W -Os -pipe \
	$(MCU_OPTS)

.PHONY: __all clean

__all:
	@echo -e "\x1b[33;1mentering lib ...\x1b[0m"
	@make -C lib
	@echo -e "\x1b[33;1mentering libc ...\x1b[0m"
	@make -C libc

clean:
	@make -C lib clean
	@make -C libc clean
