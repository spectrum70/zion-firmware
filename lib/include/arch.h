#ifndef __arch_h
#define __arch_h

#include <stdint.h>

/*
 * Supported mcus, to keep it simple, supporting only m4 with same
 * hw modules internal register set.
 *
 * STM32F303K8
 */

#define BASE_SCB	0xe000ed00
#define BASE_SYS_TIMER	0xe000e010

#ifdef STM32F303K8
#define __FPU_PRESENT	1
#define __FPU_USED	1
#define BASE_PWR	0x40007000
#define BASE_RCC	0x40021000
#define BASE_GPIOA	0x48000000
#define BASE_GPIOB	0x48000400
#define BASE_I2C1	0x40005400
#define BASE_I2C2	0x40005800
#define BASE_TIM1	0x40012c00
#define BASE_TIM8	0x40013400
#else
#error "no mcu has been defined, please check arch.h for suported mcus"
#endif

#define SCR_SLEEPDEEP	(1 << 2)

struct scb {
	uint32_t cpuid;
	uint32_t icsr;
	uint32_t vtor;
	uint32_t aircr;
	uint32_t scr;
	uint32_t ccr;
	uint32_t shpr1;
	uint32_t shpr2;
	uint32_t shpr3;
	uint32_t shcrs;
	uint32_t cfsr;
	uint32_t hfsr;
	uint32_t res;
	uint32_t mmar;
	uint32_t bfar;
	uint32_t afsr;
};

struct sys_timer {
	uint32_t csr;
	uint32_t rvr;
	uint32_t cvr;
	uint32_t calib;
};

#define ST_CSR_EN	(1 << 0)
#define ST_TICKINT	(1 << 1)
#define ST_CLKSOURCE	(1 << 2)

/*
 * hw modules abstraction, mintaining datasheet naming
 */
enum module {
	hm_pwr,
	hm_rcc,
	hm_gpioa,
	hm_gpiob,
	hm_i2c1,
	hm_i2c2,
	hm_tim1,
};

inline unsigned long get_base(enum module m)
{
	switch (m) {
	case hm_pwr:
		return BASE_PWR;
	case hm_rcc:
		return BASE_RCC;
	case hm_gpioa:
		return BASE_GPIOA;
	case hm_gpiob:
		return BASE_GPIOB;
	case hm_i2c1:
		return BASE_I2C1;
	case hm_i2c2:
		return BASE_I2C2;
	case hm_tim1:
		return BASE_TIM1;
	}

	/* Hard fault */
	return 0;
}

#endif /* __arch_h */