#ifndef __pwm_h
#define __pwm_h

#include <gpio.h>

void pwm_start(enum port gpio_port, int bit, int duty_cycle);
void pwm_stop(enum port gpio_port, int bit);

#endif /* __pwm_h */