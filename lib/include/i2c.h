#ifndef __i2c_h
#define __i2c_h

#include <stdint.h>

struct i2c_regs {
	uint32_t cr1;
	uint32_t cr2;
	uint32_t oar1;
	uint32_t oar2;
	uint32_t timingr;
	uint32_t timeoutr;
	uint32_t isr;
	uint32_t icr;
	uint32_t pecr;
	uint32_t rxdr;
	uint32_t txdr;
};

#define CR1_PE			(1 << 0)
#define CR1_TXIE		(1 << 1)
#define CR1_RXIE		(1 << 2)
#define CR1_NACKIE		(1 << 4)
#define CR1_TCIE		(1 << 6)

#define CR2_RD_WR		(1 << 10)
#define CR2_HEAD10R		(1 << 12)
#define CR2_START		(1 << 13)
#define CR2_STOP		(1 << 14)
#define CR2_NACK		(1 << 15)

#define ISR_TXE			(1 << 0)
#define ISR_TXIS		(1 << 1)
#define ISR_NACKF		(1 << 4)
#define ISR_TC			(1 << 6)
#define ISR_BUSY		(1 << 15)

#define TIMINGR_PRESC(x)	((x & 0xf) << 28)
#define TIMINGR_SCLL(x)		(x & 0xff)
#define TIMINGR_SCLH(x)		((x & 0xff) << 8)
#define TIMINGR_SDADEL(x)	((x & 0xf) << 16)
#define TIMINGR_SCLDEL(x)	((x & 0xf) << 20)


void init_i2c(int idx);
void i2c_write(uint8_t addr, uint8_t *buff, uint8_t len);

#endif  /* __i2c_h */
