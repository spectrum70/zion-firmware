#ifndef __llist_h
#define __llist_h

struct llist_s {
	char *data;
	struct llist_s * next;
};

#endif /* __llist_h */