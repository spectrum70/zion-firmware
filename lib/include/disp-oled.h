#ifndef __disp_oled_h
#define __disp_oled_h

void init_disp_oled(void);
void disp_oled_clear(void);
void disp_oled_write_at_cursor(const unsigned char *c, int col, int row);
void disp_oled_draw_imgae(int offset, char *image, int width, int height);
void disp_oled_set_font_dim(int width, int height);
void disp_oled_off(void);

#endif /* __disp_oled_h */
