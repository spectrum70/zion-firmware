#ifndef __font_h
#define __font_h

struct font_desc {
	char *name;
	int width;
	int height;
	const unsigned char *data;
};

#endif /* __font_h */
