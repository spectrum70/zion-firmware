#ifndef __display_h
#define __display_h

struct display_device {
	void (*clear)(void);
	void (*write_at)(const unsigned char *, int, int);
	void (*draw_image)(int, char *, int, int);
	void (*set_font_dim)(int, int);
	void (*update)(void);
};

void init_display(void);
void display_register(struct display_device *dev);
void display_image(int offset, char *image, int width, int height);
struct display_device *display_get_current(void);

#endif /* __display_h */
