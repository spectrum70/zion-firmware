#ifndef __dht11_h
#define __dht11_h

#include <gpio.h>

struct th {
	int hi;
	int hd;
	int ti;
	int td;
};

void init_dht11(enum port semsor_port, int pos);
int dht11_read(struct th *th);

#endif /* __dht11_h */
