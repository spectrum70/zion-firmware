/* (C) 2020 - Angelo Dureghello, generic start for stmn32 cortex-m4 */

	.syntax unified
	.cpu cortex-m4
	.thumb

	/* Cortex-m4 only supports thumb (thumb-2) state */

	.global vectors
	.global reset_handler
	.global fault
	.global event

	.word _data_start_init
	.word _data_start
	.word _data_end
	.word _bss_start
	.word _bss_end

	.section .text.reset_handler
	.weak reset_handler
	.type reset_handler, %function

reset_handler:
        /* First, copying initialized data (.data) to sram */
	ldr	r0, =_data_start_init
	ldr	r1, =_data_start
	ldr	r2, =_data_end
	b	data_init_entry
data_init_loop:
	ldr	r3, [r0], #4
	str	r3, [r1], #4
data_init_entry:
	cmp	r1, r2
	bcc	data_init_loop

	/* Second, zeroing .bss */
	movs	r0, #0
	ldr	r1, =_bss_start
	ldr	r2, =_bss_end
	b	bss_init_entry
bss_init_loop:
	str	r0, [r1], #4
bss_init_entry:
	cmp	r1, r2
	bcc	bss_init_loop

	/* General system init */
	bl	__system_init

	/* Call the application's entry point.*/
	bl	main
	bx	lr

.size reset_handler, .-reset_handler

	/*
	 * NOTE: weak and type are mandatory, since
	 * in thumb mode, .type function is mandatory since it transforms
	 * function offset to address + 1 (thumb valid address)
	 */
	.section .text.vect_handlers,"ax",%progbits

	.weak fault
	.type fault, %function
fault:
	b	fault

	.weak event
	.type event, %function
event:
	/*
	 * cortex-m specific: ARM implements
	 * ARM Architecture Procedure Calling Standard (AAPCS):
	 * the hardware will automatically stack the registers that
	 * are so caller-save. The hardware then set into lr (link register)
	 * the EXC_RETURN value, so we just need to bx lr.
	 */
	push	{lr}
	ldr	r3, =0xE000ED04
	ldr	r0, [r3]
	ldr	r1, =0x1ff
	and	r0, r0, r1
	bl	generic_irq_handler
	pop	{lr}
	bx	lr

	.weak svcall
	.type svcall, %function
svcall:
	/* swi handler, we have auto-pushed 24 bytes, r0-r3, r12, lr */
	mov	r1,  sp
	ldr	r3, [r1, #24]		/* get pc from stack, at -24 */
	ldrh	r0, [r3, #-2]		/* load halfword to get swi n. */
	bic	r0, r0, #0xFF00		/* extract swi n. */
	push	{lr}
	bl	svcall_irq_handler
	pop	{lr}			/* retrieve lr back from stack */
	stm	sp, {r0}		/* set return value */
	bx	lr

	.section .isr_vector,"a",%progbits
	.type vectors, %object
	.size vectors, .-vectors

vectors:
	.long _stack_end
	.long reset_handler
	.long fault /* NMI */
	.long fault /* HardFault */
	.long fault /* MemManage */
	.long fault /* BusFault */
	.long fault /* UsageFault */
	.long fault /* reserved */
	.long fault /* reserved */
	.long fault /* reserved */
	.long fault /* reserved */
	.long svcall /* SVCall */
	.long fault /* reserved */
	.long fault /* reserved */
	.long fault /* PendSV */
	.long event /* SysTick */

	/* The rest, from 0 to 81, should be 82 in total .. */
	.long fault, fault, fault, fault, fault, fault, fault, fault
	.long fault, fault, fault, fault, fault, fault, fault, fault
	.long fault, fault, fault, fault, fault, fault, fault, fault
	.long fault, fault, fault, fault, fault, fault, fault, fault
	.long fault, fault, fault, fault, fault, fault, fault, fault
	.long fault, fault, fault, fault, fault, fault, fault, fault
	.long fault, fault, fault, fault, fault, fault, fault, fault
	.long fault, fault, fault, fault, fault, fault, fault, fault
	.long fault, fault, fault, fault, fault, fault, fault, fault
	.long fault, fault, fault, fault, fault, fault, fault, fault
	.long fault, fault

	/* Handlers will be managed later by a single funtion. */

/* (C) 2020 Angelo Dureghello ! - end of file */