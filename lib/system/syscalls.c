/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */

extern long __HEAP_START;
extern long __HEAP_SIZE;

long sysc_get_heap_start(void)
{
	return (long)&__HEAP_START;
}

int sysc_get_heap_size(void)
{
	return (int)&__HEAP_SIZE;
}

/* Getting values from linker script */
long svcall_irq_handler(unsigned svc)
{
	long rval;

	switch(svc) {
	case 0:
		rval = sysc_get_heap_start();
		break;
	case 1:
		rval = sysc_get_heap_size();
		break;
	default:
		rval = -1;
		break;
	}

	return rval;
}
