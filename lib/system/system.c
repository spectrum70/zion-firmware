/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <arch.h>
#include <clock.h>

void __system_init(void)
{
	volatile struct rcc *rcc = (volatile struct rcc *)get_base(hm_rcc);

	/* Reset the RCC clock configuration to the default reset state */
        rcc->cr |= (uint32_t)0x00000001;

	/* Reset CFGR register */
        rcc->cfgr = 0x00000000;

        /* Reset HSEBYP bit */
        rcc->cr &= (uint32_t)0xfffbffff;

        /* Disable all interrupts */
        rcc->cir = 0x00000000;
}