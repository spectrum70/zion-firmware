/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <time.h>
#include <arch.h>
#include <clock.h>
#include <stdint.h>

#define CORRECTION_FACTOR	175

static uint32_t	sys_ticks;
static uint32_t	sys_secs;
static uint32_t sys_msecs;
struct systime_t tm;

void init_sys_timer(void)
{
	volatile struct sys_timer *st =
		(volatile struct sys_timer *)BASE_SYS_TIMER;

	st->csr = 0;

#ifdef CLOCK_SPEED_8MHZ
	st->rvr	= 7999;
#endif
	st->cvr = 0;

	st->csr |= ST_TICKINT;
	st->csr |= ST_CLKSOURCE;
	st->csr |= ST_CSR_EN;
}

void irq_handler_system_ticks(void)
{
	sys_ticks++;
	sys_msecs++;

	if (sys_ticks == 1000) {
		sys_ticks = 0;
		tm.sec++;
		sys_secs++;
		if (tm.sec == 60) {
			tm.sec = 0;
			tm.min++;
			if (tm.min == 60) {
				tm.min = 0;
				tm.hour++;
				if (tm.hour == 24) {
					tm.hour = 0;
					tm.day++;
				}
			}
		}
	}
}

uint32_t get_sys_secs(void)
{
	return sys_secs;
}

uint32_t get_sys_msecs(void)
{
	return sys_msecs;
}

/*
 * Rudimental blocking pauses
 */
#pragma GCC optimize("O3")
void delay_ms(int ms)
{
	int loops;
#ifdef CLOCK_SPEED_8MHZ
	loops = (800 - CORRECTION_FACTOR) * ms;
#endif

	while (loops--) {
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
	}
}

void delay_us(int us)
{
#ifdef CLOCK_SPEED_8MHZ
	while (us--) {
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
	}
#endif
}

#pragma GCC reset_options