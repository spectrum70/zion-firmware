/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <arch.h>
#include <clock.h>
#include <gpio.h>


void clock_enable(enum periph n)
{
	volatile struct rcc *rcc = (volatile struct rcc *)get_base(hm_rcc);
	unsigned int periph = n % 32;
	unsigned int bit = (1 << periph);

	if (n < 32)
		rcc->ahbenr |= bit;
	else if (n < 64)
		rcc->apb2enr |= bit;
	else
		rcc->apb1enr |= bit;
}

/*
 * Power modes: by default CPU is in run mode,
 * other modes are "sleep", "stop" or "stdby".
 */
static void __pre_idle(void)
{
	int i, x;

	for (i = port_a; i <= port_b; ++i) {
		for (x = 0; x < 16; ++x) {
			gpio_set_direction(i, x, dir_out);
			gpio_set_value(i, x, 1);
		}
	}
}

/*
 * To enter sleep. wfi, or wfe
 * See RM 7.3
 */
void power_enter_sleep(void)
{
	__pre_idle();

	asm volatile("wfi");
}

 /*
 * To enter stop, wfi, or wfe
 * plus othert bits:
 * PDDS and LPDS EXTI bits + SLEEPDEEP bit
 * See RM 7.3
 */
void power_enter_stop(void)
{
	volatile struct pwr *pwr = (volatile struct pwr *)get_base(hm_pwr);
	volatile struct scb *scb = (volatile struct scb *)BASE_SCB;
	__pre_idle();

	pwr->cr |= PWR_LPDS;
	pwr->cr |= PWR_PDDS;

	/* sleepdeep but */
	scb->scr |= SCR_SLEEPDEEP;
	/* go */
	asm volatile("wfi");
}