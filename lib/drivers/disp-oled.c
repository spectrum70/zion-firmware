/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <disp-oled.h>
#include <i2c.h>
#include <time.h>
#include <display.h>

#include <string.h>

#define DISP_I2C_ADDRESS		0x3c

#define DISP_WIDTH			128
#define DISP_HEIGHT			64
#define DISP_BUFF_SIZE			(DISP_WIDTH * DISP_HEIGHT / 8)

#define BYTE_CMD			0x00
#define BYTE_DATA			0x01

#define CMD_SET_MEM_MODE		0x20
#define CMD_SET_COL_ADDR		0x21
#define CMD_SET_PAGE_ADDR		0x22
#define CMD_SET_DEACT_SCROLL		0x2e

#define CMD_SET_DISP_START_LINE		0x40
#define CMD_SET_CONTRAST_CTRL		0x81
#define CMD_SET_CHARGE_PUMP		0x8d
#define CMD_SET_SEG_REMAP		0xa0
#define CMD_SET_DISP_ENTIRE_ON		0xa4
#define CMD_SET_DISP_NORMAL		0xa6
#define CMD_SET_DISP_INVERSE		0xa7
#define CMD_SET_MUX_RATIO		0xa8
#define CMD_SET_DISP_OFF		0xae
#define CMD_SET_DISP_ON			0xaf
#define CMD_SET_COM_OUT_DIR		0xc8
#define CMD_SET_DISP_OFFS		0xd3
#define CMD_SET_COM_PINS		0xda
#define CMD_SET_VCOM_DET		0xdb
#define CMD_SET_OSC_FREQ		0xd5
#define CMD_SET_PRECHARGE		0xd9

#define MEM_MODE_HORIZ			0x00

/*
 * framebuffer
 */
static char fb[DISP_BUFF_SIZE];
static char transf[8] = {0};
static int fw, fh;

/*
 * This SSD1306 needs <= 400Khz i2c speed and
 * 1,3 us ide time between commands.
 */

static inline void cmd_set_mux_ratio(uint8_t val)
{
	uint8_t cmd[3] = {BYTE_CMD, CMD_SET_MUX_RATIO};

	cmd[2] = val;

	i2c_write(DISP_I2C_ADDRESS, cmd, 3);
}

static inline void cmd_set_disp_offs(uint8_t offs)
{
	uint8_t cmd[3] = {BYTE_CMD, CMD_SET_DISP_OFFS};

	cmd[2] = offs;

	i2c_write(DISP_I2C_ADDRESS, cmd, 3);
}

static inline void cmd_set_start_line(int line)
{
	uint8_t cmd[2] = {BYTE_CMD};

	cmd[1] = CMD_SET_DISP_START_LINE + line;

	i2c_write(DISP_I2C_ADDRESS, cmd, 2);
}

static inline void cmd_set_seg_remap(void)
{
	uint8_t cmd[2] = {BYTE_CMD, CMD_SET_SEG_REMAP | 1};

	i2c_write(DISP_I2C_ADDRESS, cmd, 2);
}

static inline void cmd_set_com_out_dir()
{
	uint8_t cmd[2] = {BYTE_CMD, CMD_SET_COM_OUT_DIR};

	i2c_write(DISP_I2C_ADDRESS, cmd, 2);
}

static inline void cmd_set_com_pins(uint8_t conf)
{
	uint8_t cmd[3] = {BYTE_CMD, CMD_SET_COM_PINS};

	cmd[2] = conf;

	i2c_write(DISP_I2C_ADDRESS, cmd, 3);
}

static inline void cmd_set_contrast(uint8_t val)
{
	uint8_t cmd[3] = {BYTE_CMD, CMD_SET_CONTRAST_CTRL};

	cmd[2] = val;

	i2c_write(DISP_I2C_ADDRESS, cmd, 3);
}

static inline void cmd_entire_on(void)
{
	uint8_t cmd[2] = {BYTE_CMD, CMD_SET_DISP_ENTIRE_ON};

	i2c_write(DISP_I2C_ADDRESS, cmd, 2);
}

static inline void cmd_normal(void)
{
	uint8_t cmd[2] = {BYTE_CMD, CMD_SET_DISP_NORMAL};

	i2c_write(DISP_I2C_ADDRESS, cmd, 2);
}

static inline void cmd_set_osc_freq(uint8_t val)
{
	uint8_t cmd[3] = {BYTE_CMD, CMD_SET_OSC_FREQ};

	cmd[2] = val;

	i2c_write(DISP_I2C_ADDRESS, cmd, 3);
}

static inline void cmd_set_charge_pump(uint8_t val)
{
	uint8_t cmd[3] = {BYTE_CMD, CMD_SET_CHARGE_PUMP};

	cmd[2] = val;

	i2c_write(DISP_I2C_ADDRESS, cmd, 3);
}

static inline void cmd_set_precharge(uint8_t val)
{
	uint8_t cmd[3] = {BYTE_CMD, CMD_SET_PRECHARGE};

	cmd[2] = val;

	i2c_write(DISP_I2C_ADDRESS, cmd, 3);
}

static inline void cmd_set_vcom_det(uint8_t val)
{
	uint8_t cmd[3] = {BYTE_CMD, CMD_SET_VCOM_DET};

	cmd[2] = val;

	i2c_write(DISP_I2C_ADDRESS, cmd, 3);
}

static inline void cmd_set_disp_on(void)
{
	uint8_t cmd[2] = {BYTE_CMD, CMD_SET_DISP_ON};

	i2c_write(DISP_I2C_ADDRESS, cmd, 2);
}

static inline void cmd_set_disp_off(void)
{
	uint8_t cmd[2] = {BYTE_CMD, CMD_SET_DISP_OFF};

	i2c_write(DISP_I2C_ADDRESS, cmd, 2);
}

static inline void set_col_range(uint8_t col_start, uint8_t col_end)
{
	uint8_t cmd[4] = {BYTE_CMD, CMD_SET_COL_ADDR, col_start, col_end};

	i2c_write(DISP_I2C_ADDRESS, cmd, 4);
}

static inline void set_page_range(uint8_t start, uint8_t end)
{
	uint8_t cmd[4] = {BYTE_CMD, CMD_SET_PAGE_ADDR, start, end};

	i2c_write(DISP_I2C_ADDRESS, cmd, 4);
}

static inline void set_memory_mode(uint8_t mode)
{
	uint8_t cmd[3] = {BYTE_CMD, CMD_SET_MEM_MODE, mode};

	i2c_write(DISP_I2C_ADDRESS, cmd, 3);
}

static inline void set_scroll_off(void)
{
	uint8_t cmd[2] = {BYTE_CMD, CMD_SET_DEACT_SCROLL};

	i2c_write(DISP_I2C_ADDRESS, cmd, 2);
}

/*
 * The GDDRAM is a bit mapped static RAM holding the bit pattern to be
 * displayed. The size of the RAM is 128 x 64 bits and the RAM is divided into
 * eight pages, from PAGE0 to PAGE7, which are used for
 * monochrome 128x64 dot matrix display, as shown in Figure 8-13.
 */
static inline void disp_oled_update(void)
{
	uint8_t tmp[17] = {0x40};
	int i;

	set_col_range(0, 127);
	/* 64 height lcd has 8 pages */
	/* TODO: below line cause the display to hang, to investigate */
	//set_page_range(0, 7);

	for (i = 0; i < DISP_BUFF_SIZE; i += 16) {
		memcpy(&tmp[1], &fb[i], 16);

		i2c_write(DISP_I2C_ADDRESS, tmp, 17);
	}
}

void disp_oled_clear(void)
{
	memset(fb, 0x0, DISP_BUFF_SIZE);
	disp_oled_update();
}

void disp_oled_set_font_dim(int width, int height)
{
	fw = width;
	fh = height;
}

void transform_from_std_font(const unsigned char *c)
{
	uint8_t i, x;

	for(i = 0; i < 8; ++i) {
		for(x = 0; x < 8; ++x) {
			if (*(c + i) & (1 << (7 - x)))
				*(transf + x) |= (1 << i);
			else
				*(transf + x) &= ~(1 << i);
		}
	}
}

void disp_oled_write_at_cursor(const unsigned char *c, int col, int row)
{
	int i;
	char *dest = fb + (row * DISP_WIDTH) + col * 8;

	transform_from_std_font(c);

	for (i = 0; i < 8; i++)
		*dest++ = transf[i];
}

/*
 * A translation is needed
 *
 * fb is organized as
 * 0 ---> Width
 * ||||||||  1 byte per line
 * ||||||||
 */
static inline char compose_vertical_byte(char *image, int mask, int row_bytes)
{
	char b = 0;

	if (*image & mask)
		b |= (1 << 0);
	image += row_bytes;
	if (*image & mask)
		b |= (1 << 1);
	image += row_bytes;
	if (*image & mask)
		b |= (1 << 2);
	image += row_bytes;
	if (*image & mask)
		b |= (1 << 3);
	image += row_bytes;
	if (*image & mask)
		b |= (1 << 4);
	image += row_bytes;
	if (*image & mask)
		b |= (1 << 5);
	image += row_bytes;
	if (*image & mask)
		b |= (1 << 6);
	image += row_bytes;
	if (*image & mask)
		b |= (1 << 7);

	return b;
}

void disp_oled_draw_imgae(int offset, char *image, int width, int height)
{
	char *dest, *img;
	int x, y, row_bytes;

	row_bytes = width >> 3;
	dest = fb + offset;

	height >>= 3;

	for (y = 0; y < height; ++y) {
		img = image + (y * width);
		for (x = 0; x < row_bytes; ++x) {
			*dest++ = compose_vertical_byte(img, 0x01, row_bytes);
			*dest++ = compose_vertical_byte(img, 0x02, row_bytes);
			*dest++ = compose_vertical_byte(img, 0x04, row_bytes);
			*dest++ = compose_vertical_byte(img, 0x08, row_bytes);
			*dest++ = compose_vertical_byte(img, 0x10, row_bytes);
			*dest++ = compose_vertical_byte(img, 0x20, row_bytes);
			*dest++ = compose_vertical_byte(img, 0x40, row_bytes);
			*dest++ = compose_vertical_byte(img, 0x80, row_bytes);
			img++;
		}
	}
	disp_oled_update();
}

struct display_device oled_display_drv = {
	.clear = disp_oled_clear,
	.write_at = disp_oled_write_at_cursor,
	.draw_image = disp_oled_draw_imgae,
	.set_font_dim = disp_oled_set_font_dim,
	.update = disp_oled_update,
};

void disp_oled_off(void)
{
	cmd_set_disp_off();
}

void init_disp_oled(void)
{
	display_register(&oled_display_drv);

	memset(fb, 0x0, DISP_BUFF_SIZE);

	delay_ms(100);

	cmd_set_disp_off();
	delay_us(3);
	cmd_set_osc_freq(0x80);
	delay_us(3);
	cmd_set_mux_ratio(0x3f);
	delay_us(3);
	cmd_set_disp_offs(0);
	delay_us(3);
	cmd_set_start_line(0);
	delay_us(3);
	cmd_set_charge_pump(0x14);
	delay_us(3);
	set_memory_mode(MEM_MODE_HORIZ);
	delay_us(3);
	cmd_set_seg_remap();
	delay_us(3);
	cmd_set_com_out_dir();
	delay_us(3);

	/*
	 * Model related, critical for the odd line blank issue
	 * on 4 wires china models.
	 */
	cmd_set_com_pins(0x12);
	delay_us(3);

	cmd_set_contrast(0xcf);
	delay_us(3);
	cmd_set_precharge(0xf1);
	delay_us(3);
	cmd_set_vcom_det(0x40);
	delay_us(3);

	set_scroll_off();
	delay_us(3);
	cmd_entire_on();
	delay_us(3);
	cmd_normal();
	delay_us(3);

	cmd_set_disp_on();
	delay_us(3);

	disp_oled_update();
}
