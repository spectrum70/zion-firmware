/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pwm.h>
#include <arch.h>
#include <time.h>

/* Using Advanced-control timers */

void init_pwm(void)
{
}

static void pwm_enable_ch1(int polarity, int negated_output)
{
	volatile struct timer_ac *tim =
		(volatile struct timer_ac *)get_base(hm_tim1);

	/* Channel 1 is configured as output */
	tim->ccmr1 &= ~(CCMR1_CC1S_MASK);
	/* Channel 1 normal or negated pin and polarity */
	if (negated_output) {
		tim->ccer |= CCER_CC1NE;
		if (polarity)
			tim->ccer |= CCER_CC1NP;
	} else {
		tim->ccer |= CCER_CC1E;
		if (polarity)
			tim->ccer |= CCER_CC1P;
	}
	/* PWM mode 1 */
	tim->ccmr1 &= ~(CCMR1_OC1M_0 | CCMR1_OC1M_3);
	tim->ccmr1 |= CCMR1_OC1M_1 | CCMR1_OC1M_2;
}

static void pwm_disable_ch1(void)
{
	volatile struct timer_ac *tim =
		(volatile struct timer_ac *)get_base(hm_tim1);

	tim->ccmr1 &= ~(CCMR1_OC1M_1 | CCMR1_OC1M_2);
}

static void pwm_init_pa7(void)
{
	gpio_alternate_func(port_a, 7, 6);
	pwm_enable_ch1(0, 1);
}

static void pwm_stop_pa7(void)
{
	pwm_disable_ch1();
}

static void pwm_setup_duty_cycle(int duty_cycle)
{
	volatile struct timer_ac *tim =
		(volatile struct timer_ac *)get_base(hm_tim1);

	tim->psc = 0;
	tim->arr = duty_cycle;

	// Start PWM duty for channel 1
	tim->ccr1 = duty_cycle / 2;
	tim->bdtr = BDTR_MOE | BDTR_AOE | BDTR_OSSR;

	/* Go, counter enable */
	tim->cr1 = CR1_CEN;
}

static void pwm_stop_timer(void)
{
	volatile struct timer_ac *tim =
		(volatile struct timer_ac *)get_base(hm_tim1);

	/* Go, counter stop */
	tim->cr1 &= ~CR1_CEN;
}

/*
 * PCO    TIM1_CH1
 * PC1    TIM1_CH2
 * PC2    TIM1_CH3
 * PC3    TIM1_CH4
 * PA7    TIM1_CH1N
 */
void pwm_start(enum port gpio_port, int bit, int duty_cycle)
{
	switch (gpio_port) {
	case port_a:
		if (bit == 7)
			pwm_init_pa7();
		break;
	default:
	case port_b:
		return;
	}

	pwm_setup_duty_cycle(duty_cycle);
}

void pwm_stop(enum port gpio_port, int bit)
{
	switch (gpio_port) {
	case port_a:
		if (bit == 7)
			pwm_stop_pa7();
		break;
	default:
		return;
	}

	pwm_stop_timer();
}