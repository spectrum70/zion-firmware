/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <console.h>
#include <console-128x64.h>

static struct console_device *cons;

void init_console(void)
{
	console_128x64_init();
}

void console_register(struct console_device *console_dev)
{
	cons = console_dev;
}

void console_set_cursor(uint8_t col, uint8_t row)
{
	cons->set_cursor(col, row);
}

void console_write(char *text)
{
	cons->write(text);
}

void console_clear(void)
{
	cons->clear();
}
