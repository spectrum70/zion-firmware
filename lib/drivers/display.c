/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <display.h>
#include <disp-oled.h>

static struct display_device *display_driver;

void init_display(void)
{
	init_disp_oled();
}

void display_register(struct display_device *drv)
{
	display_driver = drv;
}

void display_image(int offset, char *image, int width, int height)
{
	display_driver->draw_image(offset, image, width, height);
}

struct display_device *display_get_current(void)
{
	return display_driver;
}

/* TO DO, register multiple devices */