/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <arch.h>

#define MAX_VECTORS	98

extern void irq_handler_system_ticks(void);

typedef void (*fun_ptr)(void);

static fun_ptr vect_table[MAX_VECTORS] = {
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	&irq_handler_system_ticks,
};

/*
 * Handling all interrupts from here
 */
void generic_irq_handler(int vector)
{
	if (vector > 0 && vector < MAX_VECTORS)
		vect_table[vector]();
}