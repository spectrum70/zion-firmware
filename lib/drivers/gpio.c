/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <arch.h>
#include <gpio.h>

static unsigned long __get_base(enum port p)
{
	switch (p) {
	case port_a:
		return get_base(hm_gpioa);
	case port_b:
		return get_base(hm_gpiob);
	}

	return 0;
}

void gpio_set_direction(enum port p, int bit, enum type t)
{
	volatile struct gpio *gpio = (volatile struct gpio *)__get_base(p);

	/* Input (and cleanup) */
	gpio->moder &= ~(0x3 << (bit << 1));

	if (t == dir_out)
		gpio->moder |= (1 << (bit << 1));
}

void gpio_set_value(enum port p, int bit, int value)
{
	volatile struct gpio *gpio = (volatile struct gpio *)__get_base(p);
	int pos = (1 << bit);

	if (!value)
		pos <<= 16;

	/* Atomic set clear */
	gpio->bsrr |= pos;
}

int gpio_get_value(enum port p, int bit)
{
	volatile struct gpio *gpio = (volatile struct gpio *)__get_base(p);

	return (gpio->idr >> bit) & 1;
}

void gpio_alternate_func(enum port p, int bit, int value)
{
	volatile struct gpio *gpio = (volatile struct gpio *)__get_base(p);

	/* Set alternate function */
	gpio->moder &= ~(0x3 << (bit << 1));
	gpio->moder |= (0x2 << (bit << 1));

	if (bit < 8) {
		gpio->afrl &= ~(0xf << (bit << 2));
		gpio->afrl |= (value << (bit << 2));
	} else {
		gpio->afrh &= ~(0xf << (bit << 2));
		gpio->afrh |= (value << (bit << 2));
	}
}

void gpio_set_out_type(enum port p, int bit, enum out_type otype)
{
	volatile struct gpio *gpio = (volatile struct gpio *)__get_base(p);

	if (otype == open_drain)
		gpio->otyper |= (1 << bit);
	else
		gpio->otyper &= ~(1 << bit);
}

void gpio_set_speed(enum port p, int bit, enum speed_type speed)
{
	volatile struct gpio *gpio = (volatile struct gpio *)__get_base(p);

	if (speed == low)
		gpio->ospeedr &= ~(0x3 << (bit << 1));
	else if (speed == medium)
		gpio->ospeedr &= ~(0x1 << (bit << 1));
	else
		gpio->ospeedr |= (0x3 << (bit << 1));
}

void gpio_set_pull(enum port p, int bit, enum pull pull_type)
{
	volatile struct gpio *gpio = (volatile struct gpio *)__get_base(p);

	/* Clear, no pulls */
	gpio->pupdr &= ~(0x3 << (bit << 1));

	if (pull_type == pull_up)
		gpio->pupdr |= (1 << (bit << 1));
	else if (pull_type == pull_down)
		gpio->pupdr |= (2 << (bit << 1));
}

