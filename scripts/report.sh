#!/bin/bash

if [ $# -ne 2 ]; then
	echo "2 arguments needed"
	exit 1
fi

FW_BIN=${1}
LD_SCRIPT=${2}

model=$(basename "${LD_SCRIPT}" | cut -d '.' -f1)
size=$(stat --printf="%s\n" ${FW_BIN})

flash=$(grep flash ${LD_SCRIPT} | grep LENGTH | cut -d '=' -f3)
flash=$(echo -e "${flash}" | sed -e 's/^[[:space:]]*//')

echo
echo "mcu model   : ${model}"
echo "binary size : ${size} of ${flash}"
echo
