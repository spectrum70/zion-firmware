#!/bin/sh

set -e

export PATH=/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin
export CROSS_COMPILE=/gcc-arm-none-eabi-9-2019-q4-major/bin/arm-none-eabi-

make clean
make

cd firmware/thermo
make clean
make
cd ../..

cd firmware/carlights
make clean
make
cd ../..

exit 0
