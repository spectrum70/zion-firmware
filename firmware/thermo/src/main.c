/*
 * zion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of zion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zion.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "init.h"

#include <clock.h>
#include <time.h>
#include <gpio.h>
#include <console.h>
#include <stdio.h>
#include <disp-oled.h>
#include <dht11.h>

extern struct systime_t tm;
static const int correction = 1;

static void uptime(void)
{
	console_set_cursor(0, 6);

	printf("%4dgg %02d:%02d:%02d",
		tm.day,
		tm.hour,
		tm.min,
		tm.sec);
}

static void get_temp_humidity(void)
{
	static 	struct th th;

	if (dht11_read(&th) == 0) {
		console_set_cursor(3, 3);
		printf("       ");
		console_set_cursor(3, 3);
		th.ti -= correction;
		printf("%d.%d", th.ti, th.td);
		console_set_cursor(3, 4);
		printf("       ");
		console_set_cursor(3, 4);
		printf("%d.%d", th.hi, th.hd);
	} else {
		console_set_cursor(3, 3);
		printf("---.---");
		console_set_cursor(3, 4);
		printf("---.---");
	}
}

/*
 * All unused pin to out, 0
 * LED off, i2c as input.
 */
static void setup_for_stop()
{
	int i, x;

	disp_oled_off();

	for (i = 0; i < 16; ++i) {
		gpio_set_direction(port_a, i, dir_out);
		/* All free driving low, except led */
		x = (i == 12) ? 1 : 0;
		gpio_set_value(port_a, i, x);
	}

	for (i = 0; i < 8; ++i) {
		x = (i > 5) ? dir_in : dir_out;
		gpio_set_direction(port_b, i, x);
		if (i <= 5)
			gpio_set_value(port_b, i, 0);
	}
}

int main()
{
	uint32_t secs, secs_read = 0;

	sys_init();

	/* Led light on, to read display */
	gpio_set_value(port_a, 12, 0);

	console_clear();

	printf("T/H monitor\nv.091(alpha)\n\n");
	printf("T:        *C\n");
	printf("H:        %%\n");

	uptime();
	get_temp_humidity();

	for (;;) {
		secs = get_sys_secs();

		if ((secs - secs_read) > 10) {
			secs_read = secs;

			setup_for_stop();
			power_enter_stop();
		}
	}

	return 0;
}
