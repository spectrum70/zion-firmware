/*
 * libzion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of libzion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libzion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libzion.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <gpio.h>
#include <clock.h>
#include <i2c.h>
#include <dht11.h>
#include <time.h>
#include <display.h>
#include <console.h>

#include "init.h"

void sys_init(void)
{
	clock_enable(p_iopa);
	/* For i2c to work on PBX pins, gpio clock must be activated */
	clock_enable(p_iopb);
	clock_enable(p_i2c1);

	init_sys_timer();
	init_i2c(0);

	/* Map I2C1 on pb6 and pb7 */
	gpio_alternate_func(port_b, 6, 4);
	gpio_alternate_func(port_b, 7, 4);

	/*
	 * There seems to be no gain (scope check) at least at 400Khz
	 * setting here open-drain and high-speed for i2c gpio pins.
	 * Everybody do this, as paparache, but, my idea (to be verified)
	 * is that once alternate mode is set as i2c, all is already set
	 * to the best way.
	 */

	/*
	 * Tested also enabling pull-up, i don't see any change in data shapes
	 * so likely, setting i2c as alternate function setups all.
	 */

	/* Setting up led as off */
	gpio_set_direction(port_a, 12, dir_out);
	gpio_set_value(port_a, 12, 1);

	init_display();
	init_console();
	init_dht11(port_a, 0);

	set_std_output(console_write);
}