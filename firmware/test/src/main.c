/*
 * zion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of zion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zion.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <clock.h>
#include <gpio.h>
#include <time.h>

int main()
{
	clock_enable(p_iopa);
	gpio_set_direction(port_a, 12, dir_out);
	gpio_set_value(port_a, 12, 1);

	for (;;) {
		gpio_set_value(port_a, 12, 1);
		delay_ms(200);
		gpio_set_value(port_a, 12, 0);
		delay_ms(200);

		//power_enter_stop();
	}

	return 0;
}
