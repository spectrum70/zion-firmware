#ifndef lcd_h
#define lcd_h

#include <llist.h>
#include <time.h>

extern const unsigned char image_alarm[];

struct geometry {
	int x;
	int y;
	int w;
	int h;
};

struct animation {
	struct geometry g;
	struct llist_s *d_list;
	struct llist_s *cur;
	int time_last;
	int interval_ms;
};

void lcd_init(void);
void lcd_welcome_message(void);
void lcd_lights_on_message(void);
void lcd_alarm_message(void);
struct animation *lcd_setup_animaiton(struct geometry *geom,
				char *first_frame, int time_interval);
void lcd_animaiton_add_frame(struct animation * anim, char *frame);
void lcd_animaiton_clear(struct animation * anim);
void lcd_animaiton(void);

#endif /* lcd_h */