/*
 * zion
 *
 * (C) 2020 Angelo Dureghello <angelo@sysam.it>
 *
 * This file is part of zion firmware library.
 *
 * minilibc library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * zion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with zion.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <clock.h>
#include <gpio.h>
#include <time.h>
#include <pwm.h>

#include "lcd.h"

enum {
	alarm_off,
	alarm_lights_on,
};

void system_init()
{
	init_sys_timer();

	clock_enable(p_iopa);
	/* For i2c to work on PBX pins, gpio clock must be activated */
	clock_enable(p_iopb);
	clock_enable(p_i2c1);
	clock_enable(p_tim1);

	lcd_init();
}

void set_alarm_state(int state)
{
	static int prev_state = alarm_off;
	static struct animation *alarm;

	if (state == prev_state)
		return;

	switch (state) {
	case alarm_lights_on: {
		struct geometry geom = {0, 0, 128, 64};

		/* Buzzer here
		 * Auto reload value 8400 (PWM Period = 1ms, 1Khz) */
		pwm_start(port_a, 7, 8400);

		alarm = lcd_setup_animaiton(&geom, (char *)image_alarm, 500);
		lcd_animaiton_add_frame(alarm, 0);
		}
		break;
	case alarm_off:
		pwm_stop(port_a, 7);
		lcd_animaiton_clear(alarm);
		lcd_lights_on_message();
		break;
	}

	prev_state = state;
}

int main()
{
	int engine;

	system_init();
	gpio_set_direction(port_a, 12, dir_in);
	gpio_set_pull(port_a, 12, pull_disabled);
	/* One false read */
	gpio_get_value(port_a, 12);

	/* Some stabilizing */
	delay_ms(100);
	lcd_welcome_message();

	delay_ms(400);
	lcd_lights_on_message();

	delay_ms(400);

	for (;;) {
		engine = gpio_get_value(port_a, 12);

		if (engine) {
			set_alarm_state(alarm_lights_on);
		} else {
			set_alarm_state(alarm_off);
		}

		lcd_animaiton();
		delay_ms(10);
	}

	return 0;
}
